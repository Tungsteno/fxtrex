﻿/*! fxTrEx - v0.5.4 Beta - 2015/01/31 
* Transfer Effect Extension for Jquery UI
* Copyright 2015 tungsteno74@yahoo.it Tungsteno; Licensed MIT */

(function ($, undefined) {
    /*
    || JQuery UI Transfer Effect Extension by Valerio Baldereschi alias Tungsteno
    ||
    || This effect basically is similar to the default Transfer effect but update the start and end points at animation execution,
    || thus you can use it with dinamics objects.
    */
    var effectTransfer = $.effects.effect.transfer = function (o, done) {
        var steps = 0;
        var elem = $(this),
		target = $(o.to),
		targetFixed = target.css("position") === "fixed",
		body = $("body"),
		fixTop = targetFixed ? body.scrollTop() : 0,
		fixLeft = targetFixed ? body.scrollLeft() : 0,
		endPosition = target.offset(),
		animation = {
		    top: endPosition.top - fixTop,
		    left: endPosition.left - fixLeft,
		    height: target.innerHeight(),
		    width: target.innerWidth()
		},
		startPosition = elem.offset(),
        trackat = o.trackAt === undefined || "" ? "none" : o.trackAt,
        trackset = (function (trType) {
            //choose the track type.
            var fnalg = [], indx, indxfn,
                arraysel = trType.split(" ");
            for (indx = 0; indx < arraysel.length; ++indx) { //verificare non va?
                switch (arraysel[indx]) {
                    case "none":
                        fnalg[indx] = function (prm) { }; //dummy algorithm for none
                        break;
                    case "stObjT":
                        fnalg[indx] = function (prm) { //algorithm function for start.
                            if (prm.fx.prop === "top") {
                                var startelem = elem,
                                targetFixed = target.css("position") === "fixed",
                                fix = targetFixed ? body.scrollTop() : 0,
                                step = startelem.offset().top - fix;
                                prm.fx.start = step;
                            }
                        };
                        break;
                    case "stObjL":
                        fnalg[indx] = function (prm) { //algorithm function for start.
                            if (prm.fx.prop === "left") {
                                var startelem = elem,
                                targetFixed = target.css("position") === "fixed",
                                fix = targetFixed ? body.scrollLeft() : 0,
                                step = startelem.offset().left - fix;
                                prm.fx.start = step;
                            }
                        };
                        break;
                    case "enObjT":
                        fnalg[indx] = function (prm) { //algorithm function for end.
                            if (prm.fx.prop === "top") {
                                var targetelem = target,
                                targetFixed = target.css("position") === "fixed",
                                fix = targetFixed ? body.scrollTop() : 0,
                                step = targetelem.offset().top - fix;
                                prm.fx.end = step;
                            }
                        };
                        break;
                    case "enObjL":
                        fnalg[indx] = function (prm) { //algorithm function for end.
                            if (prm.fx.prop === "left") {
                                var targetelem = target,
                                targetFixed = target.css("position") === "fixed",
                                fix = targetFixed ? body.scrollLeft() : 0,
                                step = targetelem.offset().left - fix;
                                prm.fx.end = step;
                            }
                        };
                        break;
                    case "stCoordT":
                        fnalg[indx] = function (prm) { //algorithm function for follow X coordinate of the start object.
                            if (prm.fx.prop === "top") {
                                var startelem = elem,
                                targetFixed = target.css("position") === "fixed",
                                fix = targetFixed ? body.scrollTop() : 0,
                                step = startelem.offset().top - fix;
                                prm.fx.now = step;
                            }
                        };
                        break;
                    case "stCoordL":
                        fnalg[indx] = function (prm) { //algorithm function for follow Y coordinate of the start object.
                            if (prm.fx.prop === "left") {
                                var startelem = elem,
                                targetFixed = target.css("position") === "fixed",
                                fix = targetFixed ? body.scrollLeft() : 0,
                                step = startelem.offset().left - fix;
                                prm.fx.now = step;
                            }
                        };
                        break;
                    case "enCoordT":
                        fnalg[indx] = function (prm) { //algorithm function for follow X position of the end object.
                            if (prm.fx.prop === "top") {
                                var targetelem = target,
                                targetFixed = target.css("position") === "fixed",
                                fix = targetFixed ? body.scrollTop() : 0,
                                step = targetelem.offset().top - fix;
                                prm.fx.now = step;
                            }
                        };
                        break;
                    case "enCoordL":
                        //TODO: Problem when the object move left to right or vice versa;
                        fnalg[indx] = function (prm) { //algorithm function for follow Y position of the end object.
                            if (prm.fx.prop === "left") {
                                var targetelem = target,
                                targetFixed = target.css("position") === "fixed",
                                fix = targetFixed ? body.scrollLeft() : 0,
                                step = targetelem.offset().left - fix;
                                prm.fx.now = step;
                            }
                        };
                        break;
                    default:
                        fnalg[indx] = null; //here need raise an exception and next some try catch event, for all wrong strings. 
                };
            };
            return fnalg;
        })(trackat),
        lastPosition = { top: startPosition.top - fixTop, left: startPosition.left - fixLeft },
        transfer = $("<div class='ui-effects-transfer'></div>")
			.appendTo(document.body)
			.addClass(o.className)
			.css({
			    top: lastPosition.top,
			    left: lastPosition.left,
			    height: elem.innerHeight(),
			    width: elem.innerWidth(),
			    position: targetFixed ? "fixed" : "absolute"
			})
			.animate(animation, {
			    duration: o.duration,
			    easing: o.easing,
			    step: function (now, fx) {
			        for (indx = 0; indx < trackset.length; ++indx)
			            trackset[indx]({ fx: fx }); //function alghoritm here.
			        // alert("start: " + fx.start + "|" + "now: " + fx.end + "end: " + fx.now);
			        //steps += 1;			        
			    },
			    complete: function () {
			        //alert("step:" + steps);
			        transfer.remove();
			        done();
			    }
			});
    };
})(jQuery);